let taille = 1.6;
let poids = 100;
// Instructions de calcul
let IMC = poids / (taille * taille);

console.log("Calcul de l'IMC :")
console.log("taille (m) :", taille)
console.log("poids (kg) :", poids)
console.log("IMC =", IMC.toFixed(1))   